using System;
using UnityEngine;
using UnityEngine.AI;


public class AIController : MonoBehaviour
{
  public NavMeshAgent navMeshAgent;
  public Transform goalPosition;

  private void Start()
  {
    navMeshAgent.destination = goalPosition.position;
  }
}
