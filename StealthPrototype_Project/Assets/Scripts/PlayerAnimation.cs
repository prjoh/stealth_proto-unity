using System;
using UnityEngine;


public class PlayerAnimation : MonoBehaviour
{
  [Header("Component References")]
  public Animator playerAnimator;

  //Animation String IDs
  private int playerMovementAnimationID;
  private int playerMovementModeAnimationID;
  private int playerRollAnimationID;
  

  public void Setup(MovementMode movementMode)
  {
    SetupAnimationIDs();
    SetMovementMode(movementMode);
  }
  
  void SetupAnimationIDs()
  {
    playerMovementAnimationID = Animator.StringToHash("Movement");
    playerMovementModeAnimationID = Animator.StringToHash("MoveFast");
    playerRollAnimationID = Animator.StringToHash("Roll");
  }
  
  public void UpdateMovementAnimation(float movementBlendValue)
  {
    playerAnimator.SetFloat(playerMovementAnimationID, movementBlendValue);
  }

  public void PlayRollAnimation()
  {
    playerAnimator.SetTrigger(playerRollAnimationID);
  }
  
  //Set Data ----
  public void SetMovementMode(MovementMode movementMode)
  {
    if (movementMode == MovementMode.FAST)
      playerAnimator.SetBool(playerMovementModeAnimationID, true);
    else
      playerAnimator.SetBool(playerMovementModeAnimationID, false);
  }
}
