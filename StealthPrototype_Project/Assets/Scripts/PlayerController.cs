using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;


public enum MovementMode
{
  FAST,
  SLOW,
}

public class PlayerController : MonoBehaviour
{
  [Header("Sub Behaviours")]
  public PlayerMovement playerMovement;
  public PlayerAnimation playerAnimation;

  [Header("Input Settings")]
  public PlayerInput playerInput;

  [Header("Spawn Position")]
  public Transform playerSpawn;
  
  private Vector3 rawInputMovement;
  private MovementMode movementMode;

  // Action Maps
  private string actionMapPlayerControls = "InGameControls";
  private string actionMapMenuControls = "MenuControls";
  
  //Current Control Scheme
  private string currentControlScheme;
  
  
  // TODO: Delete
  private void Awake()
  {
    SetupPlayer();
  }
  
  //This is called from the GameManager; when the game is being setup.
  public void SetupPlayer()
  {
    currentControlScheme = playerInput.currentControlScheme;
    movementMode = MovementMode.FAST;

    transform.position = playerSpawn.position;
    transform.rotation = playerSpawn.rotation;
    
    playerMovement.Setup(movementMode);
    playerAnimation.Setup(movementMode);
  }
  
  //INPUT SYSTEM ACTION METHODS --------------

  //This is called from PlayerInput; when a joystick or arrow keys has been pushed.
  //It stores the input Vector as a Vector3 to then be used by the smoothing function.
  public void OnMovement(InputAction.CallbackContext value)
  {
    Vector2 inputMovement = value.ReadValue<Vector2>();
    rawInputMovement = new Vector3(inputMovement.x, 0, inputMovement.y); // TODO: why vector3?
  }

  //This is called from PlayerInput, when a button has been pushed, that corresponds with the 'Attack' action
  public void OnRoll(InputAction.CallbackContext value)
  {
    if (value.started)
    {
      playerAnimation.PlayRollAnimation();
      playerMovement.BeginRoll();
    }
  }
  
  //This is called from PlayerInput, when the mouse scroll wheel is actuated
  public void OnAdjustSpeed(InputAction.CallbackContext value)
  {
    float scroll = value.ReadValue<float>();
    if (scroll > 0)
    {
      playerMovement.ModifySpeed(-1.0f);
    }
    else if (scroll < 0)
    {
      playerMovement.ModifySpeed(1.0f);
    }
  }

  public void OnSwitchMovementMode(InputAction.CallbackContext value)
  {
    if (value.started)
    {
      movementMode = (MovementMode) (((int) movementMode + 1) % 2);
      playerAnimation.SetMovementMode(movementMode);
      playerMovement.SetMovementMode(movementMode);
    }
  }

  //This is called from Player Input, when a button has been pushed, that correspons with the 'TogglePause' action
  public void OnTogglePause(InputAction.CallbackContext value)
  {
    if (value.started)
    {
      //GameManager.Instance.TogglePauseState(this);
    }
  }
  
  //INPUT SYSTEM AUTOMATIC CALLBACKS --------------

  //This is automatically called from PlayerInput, when the input device has changed
  public void OnControlsChanged()
  {
    if (playerInput.currentControlScheme != currentControlScheme)
    {
      currentControlScheme = playerInput.currentControlScheme;
      InputActionRebindingExtensions.RemoveAllBindingOverrides(playerInput.currentActionMap);
    }
  }

  //This is automatically called from PlayerInput, when the input device has been disconnected and can not be identified
  //IE: Device unplugged or has run out of batteries

  public void OnDeviceLost()
  {
    // TODO: Show to player
  }

  public void OnDeviceRegained()
  {
    StartCoroutine(WaitForDeviceToBeRegained());
  }

  IEnumerator WaitForDeviceToBeRegained()
  {
    yield return new WaitForSeconds(0.1f);
    // TODO: Show to player
  }


  //Update Loop - Used for calculating frame-based data
  void Update()
  {
    UpdatePlayerMovement();
    UpdatePlayerAnimation();
  }

  void UpdatePlayerMovement()
  {
    if (playerMovement.IsRolling())
      playerMovement.UpdateRollData();
    else
      playerMovement.UpdateMovementData(rawInputMovement);
  }

  void UpdatePlayerAnimation()
  {
    playerAnimation.UpdateMovementAnimation(playerMovement.GetSpeedNormalized());
  }
  
  public void SetInputActiveState(bool gameIsPaused)
  {
    switch (gameIsPaused)
    {
      case true:
        playerInput.DeactivateInput();
        break;

      case false:
        playerInput.ActivateInput();
        break;
    }
  }

  //Switching Action Maps ----
  public void EnableGameplayControls()
  {
    playerInput.SwitchCurrentActionMap(actionMapPlayerControls);
  }

  public void EnablePauseMenuControls()
  {
    playerInput.SwitchCurrentActionMap(actionMapMenuControls);
  }


  //Get Data ----
  public InputActionAsset GetActionAsset()
  {
    return playerInput.actions;
  }

  public PlayerInput GetPlayerInput()
  {
    return playerInput;
  }
  
  
  //Collision Handling ----
  private void OnCollisionEnter(Collision other)
  {
    // TODO: Correct for Collision
    if (playerMovement.IsRolling())
    {
      playerMovement.StopRolling();
      // playerAnimation.PlayHitAnimation(); // TODO 
    }
    // else
    // {
    //   var point = other.contacts[0].point;
    //   var dir = -other.contacts[0].normal;
    //   point -= dir;
    //   RaycastHit hitInfo;
    //   if (other.collider.Raycast(new Ray(point, dir), out hitInfo, float.MaxValue))
    //   {
    //     var normal = hitInfo.normal;
    //     Debug.DrawRay(hitInfo.point, normal, Color.magenta, 15.0f);
    //     rawInputMovement = Vector3.ProjectOnPlane(rawInputMovement, normal).normalized;
    //   }
    // }
  }
}
