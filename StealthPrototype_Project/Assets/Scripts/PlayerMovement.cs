using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;


public class PlayerMovement : MonoBehaviour
{
  [Header("Component References")]
  public Rigidbody playerRigidbody;

  [Header("Movement Settings")]
  public float minModifierFast;
  public float minModifierSlow;
  public float modifierSteps;
  public float maxSpeedFast;
  public float maxSpeedSlow;
  public int accelerationFrames; // Number of frames to completely interpolate between 2 directions
  public float turnSpeed;

  [Header("Roll Settings")]
  public int rollDuration;
  public float rollDisplacement;

  // private Camera mainCamera;

  //Stored Values Movement
  private float maxSpeed;
  private float minModifier;
  private float speedModifier;
  private Vector3 currentDirection;
  private Vector3 lastVelocity;
  private Vector3 velocity;
  private int elapsedFrames;
  
  //Stored Values Rolling
  private bool isRolling;
  private Vector3 rollFrom, rollTo;

  
  public void Setup(MovementMode movementMode)
  {
    // mainCamera = CameraManager.Instance.GetGameplayCamera();
    SetMovementMode(movementMode);
    speedModifier = 1.0f;
  }

  public void UpdateMovementData(Vector3 newMovementDirection)
  {
    if (currentDirection != newMovementDirection)
      elapsedFrames = 0;
    else
      lastVelocity = velocity;
    currentDirection = newMovementDirection;

    velocity = Vector3.Lerp(lastVelocity, newMovementDirection * maxSpeed * speedModifier, (float)elapsedFrames / accelerationFrames);
    elapsedFrames = (elapsedFrames + 1) % (accelerationFrames + 1);  // reset elapsedFrames to zero after it reached (interpolationFramesCount + 1)
  }

  public void BeginRoll()
  {
    elapsedFrames = 0;
    rollFrom = transform.position;
    rollTo = rollFrom + transform.forward * rollDisplacement;
    isRolling = true;
  }

  public void UpdateRollData()
  {
    if (!isRolling)
      return;
    elapsedFrames = (elapsedFrames + 1) % (rollDuration + 1);  // reset elapsedFrames to zero after it reached (interpolationFramesCount + 1)
  }
  
  void FixedUpdate()
  {
    if (isRolling)
    {
      RollThePlayer();
    }
    else
    {
      MoveThePlayer();
      TurnThePlayer();
    }
  }

  void RollThePlayer()
  {
    float rollProgress = (float) elapsedFrames / rollDuration;
    playerRigidbody.MovePosition(Vector3.Lerp(rollFrom, rollTo, rollProgress));
    if (Mathf.Approximately(rollProgress, 1.0f))
      isRolling = false;
  }
  
  void MoveThePlayer()
  {
    playerRigidbody.MovePosition(transform.position + velocity * Time.deltaTime);
  }
    
  void TurnThePlayer()
  {
    if (velocity.sqrMagnitude > 0.01f)
    {
      Quaternion rotation = Quaternion.Slerp(playerRigidbody.rotation,
                                             Quaternion.LookRotation(Vector3.Normalize(velocity)),
                                             turnSpeed);
      playerRigidbody.MoveRotation(rotation);
    }
  }

  // Vector3 CameraDirection(Vector3 movementDirection)
  // {
  //   var cameraForward = mainCamera.transform.forward;
  //   var cameraRight = mainCamera.transform.right;
  //
  //   cameraForward.y = 0f;
  //   cameraRight.y = 0f;
  //       
  //   return cameraForward * movementDirection.z + cameraRight * movementDirection.x; 
  // }

  //Get Data ----
  // TODO: This does not account for the ACTUAL velocity traveled (e.g. running into a wall)
  public float GetSpeedNormalized()
  {
    return velocity.magnitude / maxSpeed;
  }

  public bool IsRolling()
  {
    return isRolling;
  }
  
  //Set Data ----
  public void StopRolling()
  {
    isRolling = false;
  }

  public void ModifySpeed(float direction)
  {
    speedModifier = Mathf.Max(minModifier, Mathf.Min(1.0f, speedModifier + modifierSteps * direction));
  }

  public void SetMovementMode(MovementMode movementMode)
  {
    if (movementMode == MovementMode.FAST)
    {
      minModifier = minModifierFast;
      maxSpeed = maxSpeedFast;
    }
    else
    {
      minModifier = minModifierSlow;
      maxSpeed = maxSpeedSlow;
    }
  }
  
  //Collision Handling ----
  private void OnCollisionStay(Collision other)
  {
    if (other.gameObject.CompareTag("Level"))
    {
      var point = other.contacts[0].point;
      var dir = -other.contacts[0].normal;
      point -= dir;
      RaycastHit hitInfo;
      if (other.collider.Raycast(new Ray(point, dir), out hitInfo, float.MaxValue))
      {
        var normal = hitInfo.normal;
        Debug.DrawRay(hitInfo.point, normal, Color.magenta, 15.0f);
        velocity -= Vector3.Dot(velocity, normal) * normal;
      } 
    }
  }
}
